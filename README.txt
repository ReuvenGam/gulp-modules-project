#########################################################################
#                           Readme.txt                                  #
#########################################################################

Go to src folder run npm install in the terminal.
The gulp tasks are modules in the gulp folder.
Each task have a unique file in the folder.

The config module holds the configuration settings
of all the plugins src, dist and options, each in isolated object.

The plugins module holds all node module plugins listed in the
package.json file.