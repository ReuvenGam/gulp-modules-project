

var exports = module.exports = {};

var less = 'less/',
    scriptsSrc = 'js/main/*.js',
    scriptDist = '../js/',
    mainJs = scriptDist + "/main.js",
    cssDir = '../css/',
    imagesSrc = 'images/*',
    imagesDist = '../images';


exports.less = {
    src: less + '/**/*.less',
    imports: less + '__main.less',
    dist: cssDir,
    map: './map',
    cssnano: {
        zindex: false,
        discardComments: {
            removeAll: true
        }
    }
};

exports.bootstrap = {
    import: less + '_bootstrap.less',
    dist: cssDir,
    map: './map'
};

exports.scripts = {
    src: scriptsSrc,
    dist: scriptDist,
    hint: mainJs
};

exports.del = {
    css: cssDir,
    js: mainJs,
    images: 'images'
};

exports.imagemin = {
    src: imagesSrc,
    dist: imagesDist,
    options: {
        optimizationLevel: 3,
        progessive: true,
        interlaced: true
    }
};

exports.watch = {
    less: 'less/**/*.less',
    js: scriptsSrc

};

exports.base64 = {
    src: cssDir + "/**/*.css",
    dist: cssDir,
    options: {
        maxImageSize: 33 *1024,
        debug: true,
        baseDir: './images',
        exclude: ['eot', 'woff2', 'woff', 'ttf', 'svg']
    }
};

