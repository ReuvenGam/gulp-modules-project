/**
 * Take the generated css and convert images
 * to base64 encoding, then reload page.
 */

const gulp    = require('gulp'),
      plugins = require('../plugins'),
      config  = require('../config');

var base64 = function(){
    
    return gulp.src(config.base64.src)
               .pipe(plugins.base64(config.base64.options))
               .pipe(gulp.dest(config.base64.dist))
               //.pipe(plugins.livereload());
};

gulp.task('base64', base64);
module.exports = base64;

