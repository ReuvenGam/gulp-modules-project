/**
 * Gulp less task
 * Take all less files included in import less,
 * Compile to CSS, rename stream to style.css,
 * Prefix for the last 3 browser versions,
 * Create source map and put in CSS folder on Theme root folder
 * 
 */

const gulp         = require('gulp'),
      plugins      = require("../plugins"),
      config       = require('../config');
var cache = require('gulp-cache');

var lessTask = function(){

    return gulp.src(config.less.imports)
               .pipe(plugins.sourcemaps.init())
               .pipe(plugins.less())
               .pipe(plugins.base64(config.base64.options))
               .pipe(plugins.autoprefixer("last 3 version"))
               .pipe(plugins.rename('style.css'))
               .pipe(plugins.sourcemaps.write())
               .pipe(gulp.dest(config.less.dist))
               .pipe(plugins.cssnano(config.less.cssnano))
               .pipe(plugins.rename('style.min.css'))
               .pipe(gulp.dest(config.less.dist))
               .pipe(plugins.livereload());


};


gulp.task('clear', function (done) {
    return cache.clearAll(done);
});

gulp.task('less', ['clear'], lessTask);
module.exports = lessTask;
