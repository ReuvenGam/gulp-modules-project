/**
 * Default task to run the build and
 * start watch file changes.
 */

const gulp = require('gulp');
gulp.task("default", ['build', 'watch']);
