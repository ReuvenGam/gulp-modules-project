

const gulp = require('gulp'), 
    del = require("del"), 
    config = require('../config');

const cleanCss = () => del.sync([config.del.css]);

gulp.task('clean_css', cleanCss);
module.exports = cleanCss;