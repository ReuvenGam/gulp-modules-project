/**
 * Gulp less task
 * Take all less files included in import less,
 * Compile to CSS, rename stream to style.css,
 * Prefix for the last 3 browser versions,
 * Create source map and put in CSS folder on Theme root
 */

const gulp         = require('gulp'),
      plugins      = require("../plugins"),
      config       = require('../config');

var lessRtlTask = function(){

    return gulp.src(config.less.imports)
               .pipe(plugins.sourcemaps.init())
               .pipe(plugins.less())
               .pipe(plugins.rtlcss())
               .pipe(plugins.base64(config.base64.options))
               .pipe(plugins.autoprefixer("last 3 version"))
               .pipe(plugins.rename('style-rtl.css'))
               .pipe(plugins.sourcemaps.write())
               .pipe(gulp.dest(config.less.dist))
               .pipe(plugins.cssnano(config.less.cssnano))
               .pipe(plugins.rename('style-rtl.min.css'))
               .pipe(gulp.dest(config.less.dist))
               .pipe(plugins.livereload());
};

gulp.task('less_rtl', lessRtlTask);
module.exports = lessRtlTask;
