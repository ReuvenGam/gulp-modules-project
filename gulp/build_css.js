/**
 * Task to build the CSS file, This is a bundle of tasks 
 * 
 * Compiles Less to CSS.
 * Reduce image size.
 * Convert images to base64.
 */

const gulp = require('gulp');
gulp.task('build_css', ['bootstrap', 'less', 'imagemin', "less_rtl"]);