/**
 * Reduce image size from src/images folder
 * and put them in to images on Themes root directory
 */


const gulp = require('gulp'),
      plugins = require('../plugins'),
      config = require('../config');

const imageMin = () => {
    return gulp.src(config.imagemin.src)
               .pipe(plugins.imagemin(config.imagemin.options))
               .pipe(gulp.dest(config.imagemin.dist));
};

gulp.task('imagemin', imageMin);
module.exports = imageMin;