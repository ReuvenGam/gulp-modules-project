/**
 * Gulp bootstrap task
 * Take all less files included in import less,
 * Compile to CSS, rename stream to style.css,
 * Prefix for the last 3 browser versions,
 * Create source map and put in CSS folder on Theme root
 */

const gulp         = require('gulp'),
      plugins      = require("../plugins"),
      config       = require('../config').bootstrap;


var bootstrapTask = function(){
    return gulp.src(config.import)
               .pipe(plugins.sourcemaps.init())
               .pipe(plugins.less())
               .pipe(plugins.rename('bootstrap.css'))
               .pipe(plugins.autoprefixer("last 3 version"))
               .pipe(plugins.sourcemaps.write())
               .pipe(gulp.dest(config.dist));
};


gulp.task('bootstrap', bootstrapTask);
module.exports = bootstrapTask;
