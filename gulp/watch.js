/**
 * Watch task to iterate over development files
 * for looking for changes, each change will trigger the proper task
 * and livereload via chrome LiveReload plugin
 */

const gulp    = require('gulp'),
      config  = require('../config').watch,
      plugins = require('../plugins');


var onChange = function(e){

    plugins.livereload.changed(e.path);
};

var watchTask = function(){

    plugins.livereload.listen();
    gulp.watch([config.js], ['build_js']);
    gulp.watch([config.less], ['build_css']);
};

gulp.task('watch', watchTask);
module.exports = watchTask;