
const gulp = require('gulp'),
    plugins = require("../plugins"),
    stylish = require("jshint-stylish"),
    config = require('../config');

const jsHint = () => {
    
    gulp.src(config.scripts.hint)
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter(stylish))
};

gulp.task("jshint", jsHint);
module.exports = jsHint;